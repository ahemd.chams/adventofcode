#include <bits/stdc++.h>

using namespace std;

vector<char> C = {'L', 'H', 'E', 'Q', 'P', 'T', 'B', 'D'};
vector<int> V = {5499, 5274, 13381, 17334, 133, 5739, 136764, 2239};

string r = "";
bool parse(int v, int n, int j)
{
    if (n < 0) return false;
    if (v < 0) return false;
    if (v == 0 && n == 0) return true;
    for (int i = j; i < C.size(); ++i)
    {
        bool b = parse(v - V[i], n - 1, i);
        if (b)
        {
            r += C[i];
            return true;
        }
    }
    
    return false;
}

int main()
{
    parse(426703, 20, 0);
    sort(r.begin(), r.end());
    cout << r << endl;
    return 0;
}